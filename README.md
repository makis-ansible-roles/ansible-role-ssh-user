Role Name
=========

This Ansible role creates a new system user, adds them to the sudo group, copies their SSH public key, disables SSH access for the root user, and disables password authentication.

Requirements
------------

You need to have generated SSH public key to acces your server.

Role Variables
--------------

The following variables can be set:

- new_user: The name of the user to create. Defaults to 'www'
- new_user_shell: The user's shell. Defaults to '/bin/bash'
- new_user_password: The user's password. No password by default
- path_to_publickey: The path to the SSH public key file to be copied. Defaults to ~/.ssh/id_rsa.pub

Dependencies
------------

This role has no dependencies.

Example Playbook
----------------
    - hosts: servers
      roles:
        - role: ssh-user
          vars:
            new_user: ansible
            new_user_shell: /bin/zsh
            new_user_password: encrypted_pass
            path_to_publickey: /path/to/publickey.pub

License
-------

MIT

Author Information
------------------

This role is created by Pavel Makis.
